ActiveAdmin.register Photo do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  config.batch_actions = true
  scope :all, :default => true
   permit_params :project_id, :picture, :colors, :category, :status
  index  :as => ActiveAdmin::Views::IndexAsTable do
    selectable_column
    column :id
    column :status , :as => :check_boxes
    #end
      column :picture do |picture|
        image_tag(picture.picture.square)
      end
    column :category
    column :created_at
  actions   
  end

  form do |f|
    f.inputs "Edit photo Details" do
      f.input :project_id
      f.input :category
      f.input :status
    end
    f.form_buffers.last << f.template.render('/admin/photos/photos')
    f.actions
  end

controller do
  alias_method :edit, :update
  def update
    photo = Photo.find_by_id(params[:id])
    photo.update_attributes(:category => params[:photo]["category"])
    # do what you need to here
    # then call create_user alias
    # which calls the original create
    redirect_to :action=> :index
    # or do the create yourself and don't
    # call create_user
  end
end

end
