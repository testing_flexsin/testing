ActiveAdmin.register Profile do
  permit_params :name, :approved, :category, :email, :description, :address, :city, :phone, :url, :zip

  form do |f|
    f.semantic_errors
    f.inputs 'Profil' do
      f.input :name
      f.input :approved
      f.input :address
      f.input :zip
      f.input :city
      f.input :phone
      f.input :category
      f.input :email
      f.input :url
      f.input :description

    end
    f.actions
  end
end
