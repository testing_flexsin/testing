ActiveAdmin.register User do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
   permit_params :email, :password, :password_confirmation, :current_sign_in_ip, :last_sign_in_ip, :provider,
   :professional, :admin, :uid, :name, :birthday

  form do |f|
    f.inputs "Edit User Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :provider
      f.input :current_sign_in_ip
      f.input :last_sign_in_ip
      f.input :professional
      f.input :admin
      f.input :uid
      f.input :name
      f.input :birthday
    end
    f.actions
  end
  
end
