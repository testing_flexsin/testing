class Project < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include PublicActivity::Common
  include Facets
  extend FriendlyId

  has_many :photos 
  belongs_to :author, class_name: User
  has_one :profile, through: :author
  accepts_nested_attributes_for :photos

  default_scope { where(self.quoted_table_name + '.deleted_at IS NULL').
                  order(self.quoted_table_name + '.created_at DESC') }
  scope :filter_by_scopes, lambda { |scopes| find_by_scopes(scopes,@@valid_scopes) }
  scope :with_colors, ->(colors) { joins(:photos).merge Photo.by_colors(colors) }
  scope :published, -> { joins(:profile).merge(Profile.approved).where(self.quoted_table_name + '.published_at IS NOT NULL') }
  scope :staff_picked, -> { where(self.quoted_table_name + '.staff_picked_at IS NOT NULL') }
  scope :r, lambda { |id| joins(:photos).merge Photo.r(id) }
  scope :s, lambda { |id| where(style: Style.find_by_slug(id)) }
  scope :ort, lambda { |id| where(self.quoted_table_name + '.location ILIKE ?', "%#{id}%") }

  @@valid_scopes = %w(r s ort)

  enumerate :style
  acts_as_taggable_on :tags
  acts_as_commentable
  acts_as_likeable
  #paginates_per 2
  mount_uploader :cover_photo, PictureProjectUploader
  friendly_id :name_and_style, use: :slugged

  validates_inclusion_of :style, in: Style, allow_nil: true
  validates_presence_of :cover_photo, on: :update, unless: :stage_one?
  validates_presence_of :name, :style, unless: :stage_two?
  validates_length_of :photos, minimum: 1, if: :stage_three?
  # validates :url, url: true

  def name_and_style
    [name, style].compact.join(' - ')
  end

  def should_generate_new_friendly_id?
    name_changed? or style_changed? or super
  end


  def tags
    tags.pluck(:name)
  end

  def add_photo(photo)
    photos << photo
  end

  def add_comment(comment)
    comment_threads << comment
  end

  def likes_count
    likers(User).count
  end

  def comments_count
    comment_threads.count
  end

  def stage
    stage_one? ? 1 : stage_two? ? 2 : 3
  end

  def stage_one?
    name.blank? || style.blank?
  end

  def stage_two?
    cover_photo.url.nil? and not stage_one?
  end

  def stage_three?
    not stage_one? and not stage_two? and not cover_photo_changed?
  end

  def has_photos?
    photos.any?
  end

  def delete!
    touch :deleted_at
  end

  def to_jq_upload
    {
      'name'          => read_attribute(:cover_photo),
      'size'          => cover_photo.size,
      'url'           => cover_photo.url,
      'thumbnail_url' => cover_photo.square.url,
      'delete_url'    => remove_cover_photo_project_path(self),
      'delete_type'   => 'DELETE'
    }
  end
end
