module Facets
  extend ActiveSupport::Concern
  module ClassMethods
    def scopes_to_hash ary
      Hash[ary.to_a.in_groups_of(2)]
    end

    def find_by_scopes(filters,valid_scopes)
      return scoped if filters.nil?

      filters.in_groups_of(2).inject(scoped) do |combined_scope,scope_filter| 
        scope_name,scope_value = scope_filter
        if valid_scopes.include? scope_name
          combined_scope.send(scope_name.intern, scope_value)
        else
          combined_scope
        end
      end
    end
  end
end
