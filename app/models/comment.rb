class Comment < ActiveRecord::Base
  include PublicActivity::Common

  acts_as_nested_set scope: [:commentable_id, :commentable_type]
  default_scope { where(self.quoted_table_name + ".deleted_at IS NULL") }

  validates :body, :presence => true
  validates :user, :presence => true
  belongs_to :commentable, :polymorphic => true
  belongs_to :user
  attr_accessor :parent_id

  scope :find_comments_by_user, lambda { |user|
    where(user_id: user.id).order('created_at DESC')
  }

  scope :find_comments_for_commentable, lambda { |commentable_str, commentable_id|
    where(commentable_type: commentable_str.to_s, commentable_id: commentable_id).order('created_at DESC')
  }

  def has_children?
    self.children.any?
  end

  def delete
    touch :deleted_at
  end

  def self.build_from(obj, user, body)
    new commentable: obj, body: body, user: user
  end

  def self.create_from(obj, user, body)
    comment = build_from(obj, user, body)
    comment.save
    comment
  end

  def self.find_commentable(commentable_str, commentable_id)
    commentable_str.constantize.find(commentable_id)
  end
end
