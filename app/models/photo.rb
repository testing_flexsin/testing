class Photo < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include PublicActivity::Common

  belongs_to :project
  scope :by_colors, ->(colors) { where.contains(colors: colors) }
  scope :r, lambda { |id| where(category: RoomCategory.find_by_slug(id)) }
  #scope :project_photo, -> { where(category: $demo) }
  paginates_per 12
 
  enumerate :category, with: RoomCategory
  acts_as_commentable
  acts_as_likeable
  mount_uploader :picture, PictureProjectUploader

  validates_inclusion_of :category, in: RoomCategory, allow_nil: true

   validate :picture_size_validation, :if => "picture?"  

  def picture_size_validation
    errors[:picture] << "should be less than 6 MB" if picture.size > 6.megabytes
  end

  def add_comment(comment)
    comment_threads << comment
  end
 
  def author
    project.author
  end

  def to_jq_upload
    {
      'name'          => read_attribute(:picture),
      'size'          => picture.size,
      'url'           => picture.url,
      'thumbnail_url' => picture.square.url,
      'edit_url'      => project_photo_path(project, self),
      'delete_url'    => project_photo_path(project, self),
      'delete_type'   => 'DELETE',
      'photo_id'      => self.id
    }
  end
end
