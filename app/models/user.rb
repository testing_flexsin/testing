class User < ActiveRecord::Base
  include PublicActivity::Common

  devise :database_authenticatable, :registerable, :recoverable, :rememberable,
    :trackable, :omniauthable, omniauth_providers: [:facebook]

  alias_attribute :pro, :professional
  has_one :profile
  has_many :projects, foreign_key: :author_id
  has_many :comments
  after_create :gen_profile, :subscribe_newsletter
  
  acts_as_liker
  acts_as_follower
  acts_as_followable
  has_settings do |setting|
    setting.key :notifications, defaults: { follow: true, like: true,
                                            comment: true, reply: true }
  end

  validates :email, email: true, presence: true, uniqueness: {allow_blank: true, if: :email_changed?, link: '/users/sign_in'}
  validates :password, presence: {if: :password_required?}, confirmation: {if: :password_required?}, length: {within: 8..128, allow_blank: true}

  def liked_projects(opts = {})
    likees(Project, opts)
  end

  def liked_photos(opts = {})
    likees(Photo, opts)
  end

  def following(opts = {})
    followees(User, opts)
  end

  def followers(opts = {})
    Socialization.follow_model.followers(self, User, opts)
  end

  def avatar(size=200)
    return profile.avatar_url(size) if pro? and not profile.nil?

    gravatar_id = Digest::MD5.hexdigest(email.downcase)
    ""
  end
  
  def self.find_for_facebook_oauth(auth, signed_in_resource=nil, params=[])
    user = User.where(provider: auth.provider, uid: auth.uid).first
    unless user
      user = User.create(name:     auth.extra.raw_info.name,
                         provider: auth.provider,
                         uid:      auth.uid,
                         email:    auth.info.email,
                         password: Devise.friendly_token[0,20],
                        )
      user.birthday = Date.strptime(auth.extra.raw_info.birthday, '%m/%d/%Y') unless auth.extra.raw_info.birthday.nil?
    end
    user
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def gen_profile
    create_profile(name: name) if pro? and profile.nil?
  end

  def subscribe_newsletter
  
  end

  def password_required?
    !persisted? || !password.nil? || !password_confirmation.nil?
  end
end
