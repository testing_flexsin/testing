$ ->
  $('.navbar-open').click (event) ->
    $('.navbar-menu').toggle()
    event.preventDefault()

  $('#slider').responsiveSlides
    pause: true
    nav: true

  $('#sticker').sticky
    topSpacing: 0

  $('#flash .close').click (event)->
    $(this).parent().remove()
    event.preventDefault()

  $('a').bind 'ajax:error', (event, jqXHR, ajaxSettings, thrownError) ->
    if jqXHR.status == 401
      window.location.replace('/users/sign_in')
