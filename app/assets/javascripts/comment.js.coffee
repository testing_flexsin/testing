$ ->
  $(".comments .reply a").click (event) ->
    $(this).parents('.comment').find(".reply-form").show()
    $(this).hide()
    event.preventDefault()

  $(".comments .show_children a").click (event) ->
    $(this).parent().next("ul.subcomments").show()
    $(this).hide()
    event.preventDefault()
