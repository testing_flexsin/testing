$ ->
  $('#small-slider').owlCarousel
    items: 3
    itemsMobile:  [320, 3]
    itemsTablet:  [768, 3]
    itemsDesktop: [1199,3]
  $('.project-photos div.info a').click ->
    $('.project-photos').hide()
    $('.project-details').show()
    event.preventDefault()
  $('.project-photos span.fullscreen').click ->
    $('.project-photos').css({'width': '100%'})
    $('.project-photos span.fullscreen-close').show()
    $('.project-photos span.fullscreen').hide()
    $('.project-details').hide()
    $('.nbbarmnu').hide()
    event.preventDefault()
  $('.project-photos span.fullscreen-close').click ->
    $('.project-photos').css({'width': '75% !important'})
    $('.project-photos span.fullscreen-close').hide()
    $('.project-photos span.fullscreen').show()
    $('.project-details').show()
    $('.nbbarmnu').show()
    event.preventDefault()
  $('.filter-button a').click ->
    $('.project-search .filters').show()
    event.preventDefault()
  $('.filters .underlined a').click ->
    $('.filter-form').submit()
    event.preventDefault()
  $('#basic').fileupload
    dropZone: '#upload-photos'
    dataType: 'json'
    filesContainer: $('.project-images ul.photos')
    limitMultiFileUploads: 1
    submit: (e, data)->
      $('#uploading-photos').append('<div class="loading"></div>')
    done: (e, data)->
      $('#uploading-photos div:last-child').remove()
      template = $('#photo-template').html()
      Mustache.parse(template)
      $.each(data.result, (index, file)->
        rendered = Mustache.render(template, file[0])
        $('ul.photos').append(rendered)
      )
    fail: (e, data, foo)->
      $('#uploading-photos div:last-child').remove()
      $('#photo_error').show()

  $('#cover').fileupload
    dropZone: '#upload-cover-photo'
    dataType: 'json'
    limitMultiFileUploads: 1
    limitConcurrentUploads: 1
    submit: (e, data)->
      $('#uploading-cover').append('<div class="loading"></div>')
      $('#cover .form-inputs .error').remove()
    done: (e, data)->
      $('#uploading-cover div:last-child').remove()
      template = $('#cover-template').html()
      Mustache.parse(template)
      $.each(data.result, (index, file)->
        rendered = Mustache.render(template, file[0])
        $('#cover .form-elements').hide()
        $('#cover .form-elements').html(rendered)
        $('#cover .form-elements').show()
      )
    fail: (e, data, foo)->
      $('#uploading-cover div:last-child').remove()
      $('#cover .form-elements .form-inputs').append('<span class="error">Für Titelbild nur Fotos im Querformat zulässig.</span>')
  $(document).bind 'drop dragover', (e)->
      e.preventDefault()
      return
  $('#upload-photos').click ->
    $('#basic :file').trigger('click')
  $('#upload-cover-photo').click ->
    $('#cover :file').trigger('click')
  $('div.projects').on('ajax:success', 'div.load-more a', (event, data, status, xhr) ->
    $(this).parent().replaceWith(data)
  )
  $('ul.photos').on('ajax:success', 'form.edit_photo', (event, data, status, xhr) ->
    submit = $(this).find(":submit").attr("value", "Gespeichert!")
    setTimeout (->
      $(submit).attr "value", "Speichern"
    ), 3000
  ).on('ajax:before', 'form.edit_photo', (event, data, status, xhr) ->
    submit = $(this).find(":submit").attr("value", "Speichern...")
    submit.attr('disabled','disabled')
  ).on('ajax:complete', 'form.edit_photo', (event, data, status, xhr) ->
    submit = $(this).find(":submit").removeAttr('disabled')
  )

  get_project_filter = ->
    href = $("#r").serialize() + "&" + $("#s").serialize() + "&" + $("#ort").serialize()
    href = href.replace(/&?[^=&]+=&|&?[^=&]+=$/g, "")
    href = href.replace(/&|=/g, "/")
    href = href.replace("r/", "")
    href = href.replace("s/", "")
    href = href.replace("ort/", "")
    href

  $(".projects-index form.filter-form button").click ->
    href = '/wohnideen/' + get_project_filter()
    window.location.href = href
    false
    
