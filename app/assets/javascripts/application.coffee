#= require jquery
#= require jquery_ujs
#= require jquery.simplecolorpicker
#= require jquery.fileupload
#= require jquery.sticky
#= require chosen-jquery
#= require responsiveslides.min
#= require owl.carousel.min
#= require mustache
#= require_tree .
