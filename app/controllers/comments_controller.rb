class CommentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :ensure_comment_owner, only: [:destroy]

  def create
    comment_params = params[:comment]
    @commentable_obj = Comment.find_commentable(comment_params[:commentable_type], comment_params[:commentable_id])
    @comment = Comment.create_from(@commentable_obj, current_user, comment_params[:body])
    if comment_params[:parent_id]
      @parent = Comment.find(comment_params[:parent_id])
      @comment.move_to_child_of(@parent)
    end
    if @commentable_obj.respond_to? :create_activity
      key = "#{@commentable_obj.class.name.downcase}.#{comment_params[:parent_id] ? 'comment_reply' : 'add_comment' }"
      recipient = comment_params[:parent_id] ? Comment.find(comment_params[:parent_id]).user : @commentable_obj.author
      @commentable_obj.create_activity key: key, owner: current_user, recipient: recipient
    end

    redirect_to :back
  end

  def destroy
    @comment.delete
    head :ok, :content_type => 'text/html'
  end

  private
  def ensure_comment_owner
    @comment = current_user.comments.find(params[:id])
  rescue
    redirect_to user_session_path unless @comment
  end
end
