class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :pro
  end

  def authenticate_pro!(opts={})
    user = authenticate_user! opts
    redirect_to user_session_path unless user and user.pro?
  end

  def verify_profile!
    if current_user.pro? and current_user.profile.incomplete?
      flash[:notice] = t('user.profile.incomplete')
      redirect_to edit_profile_path(current_user.id)
    end
  end
end
