class Users::RegistrationsController < Devise::RegistrationsController
  before_filter :authenticate_user!, only: [:become_pro]

  def create
    super
    unless @user.invalid?
      UserMailer.welcome_pro(@user).deliver if @user.pro?
      UserMailer.welcome_user(@user).deliver unless @user.pro?
    end
  end

  def become_pro
    redirect_to root_path if current_user.pro?
    current_user.pro = true
    current_user.gen_profile
    current_user.save
    flash[:notice] = t('user.became_pro')
    redirect_to edit_profile_path(current_user)
  end

  protected
  def after_sign_up_path_for(resource)
    if resource.pro?
      flash[:notice] = t('user.welcome.pro')
      edit_profile_path(resource, {:params1 => "became_pro"})
    else
      flash[:notice] = t('user.welcome.common')
      root_path
    end
  end

  def after_sign_in_path_for(resource)
    root_path
  end
end
