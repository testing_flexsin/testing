class LikesController < ApplicationController
  before_filter :authenticate_user!

  def like
    @likeable_obj = params[:type].constantize.find(params[:id])
    current_user.toggle_like! @likeable_obj
    if @likeable_obj.respond_to? :create_activity and current_user.likes? @likeable_obj
      key = "#{@likeable_obj.class.name.downcase}.like"
      @likeable_obj.create_activity key: key, owner: current_user, recipient: @likeable_obj.author
    end
    render layout: false
  end
end
