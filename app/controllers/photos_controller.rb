class PhotosController < ApplicationController
  before_filter :authenticate_pro!, only: [:update, :destroy]
  before_filter :ensure_project_owner, only: [:update, :destroy]
  before_filter :pick_photo

  def show
    index = @project.photos.find_index(@photo)
    @prev_photo = @project.photos[index-1]
    @next_photo = @project.photos[index+1] || @project.photos.first
    @comment = Comment.build_from(@photo, current_user, '')
    @comments = @photo.root_comments.order('created_at desc')
  end

  def update
    @photo.update(photo_params)
    flash[:notice] = 'Successfully updated photo'
    redirect_to project_photo_path @project, @photo
  end

  def destroy
    @photo.remove_picture!
    @photo.delete
    redirect_to edit_project_path(@project)
  rescue
    redirect_to root_path
  end

  private
  def photo_params
    params.require(:photo).permit(:category)
  end

  def pick_photo
    @project = Project.find(params[:project_id])
    @photo = @project.photos.find(params[:id])
  rescue
    redirect_to root_path
  end

  def ensure_project_owner
    current_user.projects.find(params[:project_id])
  rescue
    redirect_to user_session_path
  end
end
