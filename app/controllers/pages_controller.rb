class PagesController < ApplicationController
  include HighVoltage::StaticPage
  before_filter :load_notifications, :load_projects

  private
  def load_notifications
    return unless params[:id] == 'home'
    @notifications = PublicActivity::Activity.where("recipient_id = ? OR (recipient_id IS NULL AND owner_type = 'User' AND owner_id IN (SELECT followable_id FROM follows WHERE follower_id = ?))", current_user.id, current_user.id).order(created_at: :desc).limit(5) if user_signed_in?
  end

  def load_projects
    return unless params[:id] == 'home'
    @projects = Project.staff_picked.first(6)
    @projects_count = Project.staff_picked.count
    session[:home_page] = "#{request.url}"
    
  end
end
