class ProfileController < ApplicationController
  before_filter :authenticate_user!, only: [:follow, :update_profile_data]
  before_filter :authenticate_pro!, only: [:edit, :update,:update_profile_data, :avatar]
  include ProfileHelper
  def index
    facets = params[:facets] ? params[:facets].split('/') : nil
    @scopes = Profile.scopes_to_hash facets
    @order = (params[:s]) ? params[:s] : 'created_at desc'
    @search = Profile.complete.approved.filter_by_scopes(facets).order(@order)
    @profiles = @search.page(params[:page])
    @for_seo_reason = @search.limit(6).order('id desc').map{|i| i.id}
    @profiles_count = @search.count
  end

  def show
    @profile = Profile.find(params[:id])
    @projects = @profile.projects
    @projects = @projects.published unless current_user == @profile.user
    session[:profile_show] = "#{request.url}"
    session[:home_page] = nil
    
  end

  def edit
    @profile = current_user.profile
    session[:params1] = params[:params1]
    #flash.now[:info] = t('profile.pending_approval') unless @profile.approved?
  end

  def update
    @profile = current_user.profile
    #flash.now[:info] = t('profile.pending_approval') unless @profile.approved?
    ok = @profile.update(profile_params)
    respond_to do |format|
      format.html {
        if ok 
          flash.now[:success] = t('controllers.profile.update.success')
          redirect_to check_edit_proflie_helper
        else
          flash.now[:error] = t('controllers.profile.update.error')
          render :edit
        end
      }
      format.json {
        if ok 
          render json: { files: [@profile.to_jq_upload] }
        else
          render json: { files: [@profile.to_jq_upload] }
        end
      }
    end
  end

  def avatar
    @profile = current_user.profile
    @profile.remove_avatar!
    @profile.save(validate: false)
    redirect_to edit_profile_path @profile
  end

  def follow
    @profile = Profile.find(params[:id])
    current_user.toggle_follow! @profile.user
    @profile.user.create_activity key: 'user.follow', owner: current_user, recipient: @profile.user
    head :no_content
  end

  def update_profile_data
    @profile = Profile.find_by(:id => params[:id])
    @profile.attributes = {:name => params[:name],
                           :email => params[:email],
                           :description => params[:description], 
                           :address => params[:profile_address],
                           :zip =>params[:profile_zip],
                           :phone => params[:profile_phone],
                           :city => params[:profile_city]
                          }
    @profile.save(validate: false)
    redirect_to "/profil/#{@profile.id}/avatar"
  end

  private
  def profile_params
    params.require(:profile).permit(:name, :address, :zip, :city, :phone, :email, :url, :category, :description, :facebook_path, :twitter_path, :linkedin_path, :avatar)
  end
end
