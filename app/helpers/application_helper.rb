module ApplicationHelper
  def alert_class flash_key
    keys = {
      'alert'     => 'alert-warning',
      'notice'    => 'alert-warning',
      'info'      => 'alert-info',
      'secondary' => 'alert-info',
      'success'   => 'alert-success',
      'error'     => 'alert-error'
    }
    keys[flash_key.to_s]
  end

  def tags_for context
    ActsAsTaggableOn::Tag.uniq.order(:name).pluck(:name)
  end

  def home_slider
    [{url: 'home/jack.jpg', owner: 'Villa'}].shuffle.sample(5)
  end
end
