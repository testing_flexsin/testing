module PhotosHelper
  def slider_image_helper
    "/foto/#{@id}/#{@pcat}/#{@style }/#{@project.name.parameterize if @project}/#{@category}"
  end
end