module DashboardHelper
  def link_to_user user
    unless user.profile.nil?
      link_to user.name, profile_path(user.profile)
    else
      user.email
    end
  end

  def link_to_trackable(object, object_type)
    return link_to_user object if object_type == 'User'
    if object
      link_to object_type.downcase, object
    else
      "a #{object_type.downcase} which does not exist anymore"
    end
  end

  def class_for_key key
    case key
    when 'project.comment_reply' then 'glyphicon-comment'
    when 'project.add_comment' then 'glyphicon-comment'
    when 'project.create' then 'glyphicon-plus'
    when 'project.like' then 'glyphicon-star'
    when 'photo.comment_reply' then 'glyphicon-comment'
    when 'photo.add_comment' then 'glyphicon-comment'
    when 'photo.like' then 'glyphicon-star'
    when 'user.follow' then 'glyphicon-user'
    else 'glyphicon-bullhorn'
    end
  end
end
