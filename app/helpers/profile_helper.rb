module ProfileHelper
  def profile_map_image profile
    ["http://maps.googleapis.com/maps/api/staticmap",
     "&scale=2"].join
  end

  def profile_map_url profile
    "http://maps.google.com/2"
  end

  def first_profile_photo profile
    return false unless profile.instance_of? Profile
    project = profile.projects.published.first
    if project and project.cover_photo?
      return project.cover_photo.profile
    else
      return "logo1.JPG"
    end
    false
  end

  def profile_title scopes
    title = scopes.has_key?('b') ? ProCategory.name_by_slug(scopes['b']) : 'Alle experten'
    title+= scopes.has_key?('o') ? " in #{scopes['o']}" : ' in Deutschland'
    title+= ' | LOGO'
    title
  end

  def link_to_pro_category slug
    '/handwerker/' + slug
  end

  def profile_meta_keywords scopes
    return t("seo.pro_category.#{scopes['b']}.meta.keywords", raise: false) if scopes.has_key?('b')
    t('profile.meta.keywords')
  end

  def profile_meta_description scopes
    return t("seo.pro_category.#{scopes['b']}.meta.description", raise: false) if scopes.has_key?('b')
    t('profile.meta.description')
  end

  def profile_seo_text scopes
    return t("seo.pro_category.#{scopes['b']}.text", raise: false, default: '') if scopes.has_key?('b')
  end

  def check_edit_proflie_helper
    if session[:params1] == "became_pro"
      return new_project_path
    else
      return  @profile
    end
  end
end
